envName: develop

# ------------------------------------------------------------------------------
global:
  awsRegion: &aws_region eu-central-1
  awsAccountId: "236859812685"

  baseDns: coms-dev.aws.de.pri.o2.com

  database:
    rdsConsentSubDomain: dev-rds
    dbNamePrefix: consent
    srvExternalName: postgres-consent

  kafka:
    baseDns: cesp-dev.aws.de.pri.o2.com
    connectSubDomain: kafka-psv
    connectPort: "9092"
    schemaSubDomain: schema-registry
    producerSubDomain: kafka

  imagePullSecrets:
    - name: artifactory

# ------------------------------------------------------------------------------
apiConsent:
  name: api-consent

  replicaCount: 1
  revisionHistoryLimit: 2

  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 0
      maxSurge: 10%

  image: "docker.artifactory.sg.de.pri.o2.com/coms/api-consent:latest"

  servicePort: 3000
  containerPort: 3000

  env:
  - name: APP_HOST
    value: 0.0.0.0
  - name: NODE_ENV
    valueFrom:
      configMapKeyRef:
        key: envName
        name: globals
  - name: POSTGRES_HOST
    valueFrom:
      configMapKeyRef:
        key: database.consent.host
        name: globals
  - name: POSTGRES_PORT
    valueFrom:
      configMapKeyRef:
        key: database.consent.port
        name: globals
  - name: POSTGRES_DBNAME
    valueFrom:
      configMapKeyRef:
        key: database.consent.dbName
        name: globals
  - name: POSTGRES_USERNAME
    valueFrom:
      secretKeyRef:
        key: username
        name: database-consent
  - name: POSTGRES_PASSWORD
    valueFrom:
      secretKeyRef:
        key: password
        name: database-consent
  - name: AUTH_AES_KEY_BASE64
    valueFrom:
      secretKeyRef:
        key: auth_aes_key_base64
        name: jwt
  - name: AUTH_AES_KEY_BASE64_AGENT
    valueFrom:
      secretKeyRef:
        key: auth_aes_key_base64_agent
        name: jwt
  - name: AUTH_ACCESS_TOKEN_SECRET
    valueFrom:
      secretKeyRef:
        key: auth_access_token_secret
        name: jwt
  - name: AUTH_FORGEROCK_ID_TOKEN_SECRET
    valueFrom:
      secretKeyRef:
        key: secret
        name: forgerock
  - name: APIGW_USERNAME
    valueFrom:
      secretKeyRef:
        key: username
        name: apigw
  - name: DPM_USERNAME
    valueFrom:
      secretKeyRef:
        key: username
        name: dpm
  - name: DPM_PASSWORD
    valueFrom:
      secretKeyRef:
        key: password
        name: dpm
  - name: DPM_URI
    valueFrom:
      secretKeyRef:
        key: uri
        name: dpm

  podAnnotations:
    linkerd.io/inject: enabled
  deploymentAnnotations: {}
  serviceAccountAnnotations: {}

  restartPolicy: Always

  resources:
    limits:
      cpu: 250m
      memory: 512Mi
    requests:
      cpu: 50m
      memory: 150Mi

  readinessProbe:
    httpGet:
      path: /healthcheck?deep=true
      port: http
    initialDelaySeconds: 5
    periodSeconds: 15
    failureThreshold: 2
    successThreshold: 1
    timeoutSeconds: 10

  livenessProbe:
    httpGet:
      path: /healthcheck
      port: http
    initialDelaySeconds: 5
    periodSeconds: 15
    failureThreshold: 2
    successThreshold: 1
    timeoutSeconds: 10

  volumes:
  - name: tz-config
    hostPath:
      path: /usr/share/zoneinfo/Europe/Berlin

  volumeMounts:
  - name: tz-config
    mountPath: /etc/localtime
    readOnly: true

  affinity:
    podAntiAffinity:
      preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 100
        podAffinityTerm:
          labelSelector:
            matchExpressions:
            - key: app
              operator: In
              values:
              - api-consent
          topologyKey: "kubernetes.io/hostname"

  podDisruptionBudget:
    enabled: false
    minAvailable: 50%

  autoscaling:
    enabled: false
    minReplicas: 1
    maxReplicas: 48
    targetCPUUtilizationPercentage: 50

# ------------------------------------------------------------------------------
apiPartner:
  name: api-partner

  replicaCount: 1
  revisionHistoryLimit: 2

  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 0
      maxSurge: 10%

  image: "docker.artifactory.sg.de.pri.o2.com/coms/api-partner:latest"

  servicePort: 3100
  containerPort: 3100

  env:
  - name: APP_HOST
    value: 0.0.0.0
  - name: NODE_ENV
    valueFrom:
      configMapKeyRef:
        key: envName
        name: globals
  - name: POSTGRES_HOST
    valueFrom:
      configMapKeyRef:
        key: database.consent.host
        name: globals
  - name: POSTGRES_PORT
    valueFrom:
      configMapKeyRef:
        key: database.consent.port
        name: globals
  - name: POSTGRES_DBNAME
    valueFrom:
      configMapKeyRef:
        key: database.consent.dbName
        name: globals
  - name: POSTGRES_USERNAME
    valueFrom:
      secretKeyRef:
        key: username
        name: database-consent
  - name: POSTGRES_PASSWORD
    valueFrom:
      secretKeyRef:
        key: password
        name: database-consent
  - name: APIGW_BASEURI
    valueFrom:
      secretKeyRef:
        key: baseuri
        name: apigw
  - name: APIGW_AUTHURI
    valueFrom:
      secretKeyRef:
        key: authuri
        name: apigw
  - name: APIGW_CLIENTID
    valueFrom:
      secretKeyRef:
        key: clientid
        name: apigw
  - name: APIGW_CLIENTSECRET
    valueFrom:
      secretKeyRef:
        key: clientsecret
        name: apigw
  - name: API_URI
    valueFrom:
      configMapKeyRef:
        key: api.uri
        name: globals

  podAnnotations:
    linkerd.io/inject: enabled
  deploymentAnnotations: {}
  serviceAccountAnnotations: {}

  restartPolicy: Always

  resources:
    limits:
      cpu: "1"
      memory: 512Mi
    requests:
      cpu: 50m
      memory: 150Mi

  readinessProbe:
    httpGet:
      path: /healthcheck
      port: http
    initialDelaySeconds: 5
    periodSeconds: 15
    failureThreshold: 2
    successThreshold: 1
    timeoutSeconds: 10

  livenessProbe:
    httpGet:
      path: /healthcheck
      port: http
    initialDelaySeconds: 5
    periodSeconds: 15
    failureThreshold: 2
    successThreshold: 1
    timeoutSeconds: 10

  volumes:
  - name: tz-config
    hostPath:
      path: /usr/share/zoneinfo/Europe/Berlin

  volumeMounts:
  - name: tz-config
    mountPath: /etc/localtime
    readOnly: true

  affinity:
    podAntiAffinity:
      preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 100
        podAffinityTerm:
          labelSelector:
            matchExpressions:
            - key: app
              operator: In
              values:
              - api-partner
          topologyKey: "kubernetes.io/hostname"

  podDisruptionBudget:
    enabled: false
    minAvailable: 50%

  autoscaling:
    enabled: false
    minReplicas: 1
    maxReplicas: 48
    targetCPUUtilizationPercentage: 50

# ------------------------------------------------------------------------------
csApp:
  name: cs-app

  replicaCount: 1
  revisionHistoryLimit: 2

  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 0
      maxSurge: 25%

  image: "docker.artifactory.sg.de.pri.o2.com/coms/cs-app:latest"

  servicePort: 80
  containerPort: 80

  env:
  - name: API_HOST
    valueFrom:
      configMapKeyRef:
        key: api.host
        name: globals
  - name: API_PORT
    valueFrom:
      configMapKeyRef:
        key: api.servicePort
        name: globals

  podAnnotations:
    linkerd.io/inject: disabled
  deploymentAnnotations: {}
  serviceAccountAnnotations: {}

  restartPolicy: Always

  resources:
    limits:
      cpu: 50m
      memory: 256Mi
    requests:
      cpu: 10m
      memory: 32Mi

  readinessProbe:
    httpGet:
      path: /app
      port: http
    initialDelaySeconds: 5
    periodSeconds: 15
    failureThreshold: 2
    successThreshold: 1
    timeoutSeconds: 10

  livenessProbe:
    httpGet:
      path: /app
      port: http
    initialDelaySeconds: 5
    periodSeconds: 15
    failureThreshold: 2
    successThreshold: 1
    timeoutSeconds: 10

  volumes:
  - name: tz-config
    hostPath:
      path: /usr/share/zoneinfo/Europe/Berlin

  volumeMounts:
  - name: tz-config
    mountPath: /etc/localtime
    readOnly: true

  affinity:
    podAntiAffinity:
      preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 100
        podAffinityTerm:
          labelSelector:
            matchExpressions:
            - key: app
              operator: In
              values:
              - cs-app
          topologyKey: "kubernetes.io/hostname"

  podDisruptionBudget:
    enabled: false
    minAvailable: 50%

# ------------------------------------------------------------------------------
demoApp:
  name: demo-app

  replicaCount: 1
  revisionHistoryLimit: 2

  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 0
      maxSurge: 25%

  image: "docker.artifactory.sg.de.pri.o2.com/coms/demo-app:latest"

  servicePort: 80
  containerPort: 80

  env:
  - name: API_HOST
    valueFrom:
      configMapKeyRef:
        key: api.host
        name: globals
  - name: API_PORT
    valueFrom:
      configMapKeyRef:
        key: api.servicePort
        name: globals
  - name: API_USERNAME
    valueFrom:
      secretKeyRef:
        name: demo-user
        key: username
  - name: API_PASSWORD
    valueFrom:
      secretKeyRef:
        name: demo-user
        key: password

  podAnnotations:
    linkerd.io/inject: disabled
  deploymentAnnotations: {}
  serviceAccountAnnotations: {}

  restartPolicy: Always

  resources:
    limits:
      cpu: 50m
      memory: 256Mi
    requests:
      cpu: 10m
      memory: 32Mi

  readinessProbe:
    httpGet:
      path: /app
      port: http
    initialDelaySeconds: 5
    periodSeconds: 15
    failureThreshold: 2
    successThreshold: 1
    timeoutSeconds: 10

  livenessProbe:
    httpGet:
      path: /app
      port: http
    initialDelaySeconds: 5
    periodSeconds: 15
    failureThreshold: 2
    successThreshold: 1
    timeoutSeconds: 10

  volumes:
  - name: tz-config
    hostPath:
      path: /usr/share/zoneinfo/Europe/Berlin

  volumeMounts:
  - name: tz-config
    mountPath: /etc/localtime
    readOnly: true

  affinity: {}

  podDisruptionBudget:
    enabled: false
    minAvailable: 50%

# ------------------------------------------------------------------------------
houseKeeping:
  name: hks-housekeeping

  replicaCount: 1
  revisionHistoryLimit: 2

  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 0
      maxSurge: 25%

  image: "docker.artifactory.sg.de.pri.o2.com/coms/hks-housekeeping:latest"

  containerPort: 5000

  env:
  - name: NODE_ENV
    valueFrom:
      configMapKeyRef:
        key: envName
        name: globals
  - name: POSTGRES_HOST
    valueFrom:
      configMapKeyRef:
        key: database.consent.host
        name: globals
  - name: POSTGRES_PORT
    valueFrom:
      configMapKeyRef:
        key: database.consent.port
        name: globals
  - name: POSTGRES_DBNAME
    valueFrom:
      configMapKeyRef:
        key: database.consent.dbName
        name: globals
  - name: POSTGRES_USERNAME
    valueFrom:
      secretKeyRef:
        key: username
        name: database-consent
  - name: POSTGRES_PASSWORD
    valueFrom:
      secretKeyRef:
        key: password
        name: database-consent

  podAnnotations:
    linkerd.io/inject: disabled
  deploymentAnnotations: {}
  serviceAccountAnnotations: {}

  restartPolicy: Always

  resources:
    limits:
      cpu: 50m
      memory: 512Mi
    requests:
      cpu: 10m
      memory: 64Mi

  readinessProbe:
    httpGet:
      path: /healthcheck
      port: http
    initialDelaySeconds: 5
    periodSeconds: 15
    failureThreshold: 2
    successThreshold: 1
    timeoutSeconds: 10

  livenessProbe:
    httpGet:
      path: /healthcheck
      port: http
    initialDelaySeconds: 5
    periodSeconds: 15
    failureThreshold: 2
    successThreshold: 1
    timeoutSeconds: 10

  volumes:
  - name: tz-config
    hostPath:
      path: /usr/share/zoneinfo/Europe/Berlin

  volumeMounts:
  - name: tz-config
    mountPath: /etc/localtime
    readOnly: true

  affinity: {}

# ------------------------------------------------------------------------------
microFrontEndProxy:
  name: micro-frontend-proxy

  replicaCount: 1
  revisionHistoryLimit: 2

  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 0
      maxSurge: 25%

  image: "docker.artifactory.sg.de.pri.o2.com/coms/micro-frontend-proxy:latest"

  servicePort: 80
  containerPort: 3500

  env:
  - name: S3_BUCKET_NAME
    valueFrom:
      configMapKeyRef:
        key: microFrontEnd.assets.s3
        name: globals
  - name: NODE_ENV
    valueFrom:
      configMapKeyRef:
        key: envName
        name: globals
  - name: AWS_DEFAULT_REGION
    value: *aws_region
  - name: NODE_EXTRA_CA_CERTS
    value: /etc/ssl/certs/tetra.crt

  podAnnotations:
    linkerd.io/inject: disabled
  deploymentAnnotations: {}
  serviceAccountAnnotations: {}

  awsRoleNameSuffix: micro-frontend-proxy
  restartPolicy: Always

  resources:
    limits:
      cpu: 1
      memory: 2Gi
    requests:
      cpu: 10m
      memory: 64Mi

  readinessProbe:
    httpGet:
      path: /healthcheck
      port: http
    initialDelaySeconds: 120 # initial setup with S3
    periodSeconds: 15
    failureThreshold: 2
    successThreshold: 1
    timeoutSeconds: 10

  livenessProbe:
    httpGet:
      path: /healthcheck
      port: http
    initialDelaySeconds: 120 # initial setup with S3
    periodSeconds: 15
    failureThreshold: 2
    successThreshold: 1
    timeoutSeconds: 10

  volumes:
  - name: tz-config
    hostPath:
      path: /usr/share/zoneinfo/Europe/Berlin
  - name: tetra-proxy
    secret:
      secretName: tetra-proxy
      defaultMode: 420

  volumeMounts:
  - name: tz-config
    mountPath: /etc/localtime
    readOnly: true
  - name: tetra-proxy
    mountPath: /etc/ssl/certs/tetra.crt
    subPath: tetra.pem
    readOnly: true

  affinity:
    podAntiAffinity:
      preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 100
        podAffinityTerm:
          labelSelector:
            matchExpressions:
            - key: app
              operator: In
              values:
              - micro-frontend-proxy
          topologyKey: "kubernetes.io/hostname"

  podDisruptionBudget:
    enabled: false
    minAvailable: 50%

# ------------------------------------------------------------------------------
msgConnect:
  name: msg-connect

  replicaCount: 1
  revisionHistoryLimit: 2

  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 0
      maxSurge: 10%

  image: "docker.artifactory.sg.de.pri.o2.com/coms/msg-connect:latest"

  containerPort: 4000

  env:
  - name: APP_HOST
    value: 0.0.0.0
  - name: NODE_ENV
    valueFrom:
      configMapKeyRef:
        key: envName
        name: globals
  - name: POSTGRES_HOST
    valueFrom:
      configMapKeyRef:
        key: database.consent.host
        name: globals
  - name: POSTGRES_PORT
    valueFrom:
      configMapKeyRef:
        key: database.consent.port
        name: globals
  - name: POSTGRES_DBNAME
    valueFrom:
      configMapKeyRef:
        key: database.consent.dbName
        name: globals
  - name: POSTGRES_USERNAME
    valueFrom:
      secretKeyRef:
        key: username
        name: database-consent
  - name: POSTGRES_PASSWORD
    valueFrom:
      secretKeyRef:
        key: password
        name: database-consent
  - name: MESSENGER_CHUNK_SIZE
    value: "100"
  - name: MESSENGER_PARALLEL_OPS
    value: "10"
  - name: CRMLISTENER_CHUNK_SIZE
    value: "10"
  - name: CRMLISTENER_PARALLEL_OPS
    value: "5" # parallel requests to COMS API
  - name: API_URI
    valueFrom:
      configMapKeyRef:
        key: api.uri
        name: globals
  - name: API_AUTH_USERNAME
    valueFrom:
      secretKeyRef:
        key: username
        name: crmlistener-api
  - name: API_AUTH_PASSWORD
    valueFrom:
      secretKeyRef:
        key: password
        name: crmlistener-api
  - name: KAFKA_CONNECT
    valueFrom:
      configMapKeyRef:
        key: kafka.connect
        name: globals
  - name: KAFKA_SECURITY_PROTOCOL
    value: "sasl_ssl"
  - name: KAFKA_SASL_MECHANISMS
    value: "PLAIN"
  - name: KAFKA_SASL_USERNAME
    valueFrom:
      secretKeyRef:
        key: username
        name: kafka
  - name: KAFKA_SASL_PASSWORD
    valueFrom:
      secretKeyRef:
        key: password
        name: kafka
  - name: KAFKA_SSL_CA_LOCATION
    value: "/mnt/ca-bundle.crt"
  - name: KAFKA_DEBUG # https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
    value: "" # e.g., `all` or `generic,broker,security`
  - name: KAFKA_STATISTICS_INTERVAL_MS
    value: "60000" # write Kafka stats once a minute
  - name: CONFLUENT_URI # Confluent Connection URI, e.g., `http://hostname:8081`
    valueFrom:
      configMapKeyRef:
        key: kafka.schema
        name: globals
  - name: CONFLUENT_AUTH # Authorization header to be set all for Confluent requests (if necessary)
    value: "" # no authorization
  - name: AUDITLOGGER_CHUNK_SIZE
    value: "30"
  - name: AUDITLOGGER_PARALLEL_OPS
    value: "10" # creates PDF (heavy lifting) + parallel requests to Glacier
  - name: AUDITPURGER_CHUNK_SIZE
    value: "50"
  - name: AUDITPURGER_PARALLEL_OPS
    value: "5" # parallel requests to Glacier
  - name: GLACIER_VAULT
    valueFrom:
      configMapKeyRef:
        key: glacier.vaultName
        name: globals
  - name: NODE_EXTRA_CA_CERTS
    value: /etc/ssl/certs/tetra.crt

  podAnnotations:
    linkerd.io/inject: enabled
  deploymentAnnotations: {}
  serviceAccountAnnotations: {}

  awsRoleNameSuffix: msg-connect-role
  restartPolicy: Always

  resources:
    limits:
      cpu: 250m
      memory: 512Mi
    requests:
      cpu: 50m
      memory: 128Mi

  readinessProbe:
    httpGet:
      path: /healthcheck
      port: http
    initialDelaySeconds: 60 # calls to schema registry etc.
    periodSeconds: 15
    failureThreshold: 2
    successThreshold: 1
    timeoutSeconds: 10

  livenessProbe:
    httpGet:
      path: /healthcheck
      port: http
    initialDelaySeconds: 60 # calls to schema registry etc.
    periodSeconds: 15
    failureThreshold: 2
    successThreshold: 1
    timeoutSeconds: 10

  volumes:
  - name: tz-config
    hostPath:
      path: /usr/share/zoneinfo/Europe/Berlin
  - name: kafka-crt
    configMap:
      name: kafka-ca-bundle
  - name: tetra-proxy
    secret:
      secretName: tetra-proxy
      defaultMode: 420

  volumeMounts:
  - name: tz-config
    mountPath: /etc/localtime
    readOnly: true
  - name: kafka-crt
    mountPath: /mnt
    readOnly: true
  - name: tetra-proxy
    mountPath: /etc/ssl/certs/tetra.crt
    subPath: tetra.pem
    readOnly: true

  affinity:
    podAntiAffinity:
      preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 100
        podAffinityTerm:
          labelSelector:
            matchExpressions:
            - key: app
              operator: In
              values:
              - msg-connect
          topologyKey: "kubernetes.io/hostname"

  podDisruptionBudget:
    enabled: false
    minAvailable: 50%
